/*
 * 	FILE: 			rot13.c
 *  CREATED: 		16 July 2019
 *  AUTHOR: 		Raymond Thomson
 *  CONTACT: 		raymond.thomson76@gmail.com
 *  COPYRIGHT:		Free Use
 */


#define number0 (int)'0'
#define number9 (int)'a'
#define lettera (int)'a'
#define letterz (int)'z'
#define letterA (int)'A'
#define letterZ (int)'Z'

#include <stdio.h>
#include <unistd.h>

int main (void) {
	char ch;
	char buf[BUFSIZ];
	int tc;

	while ((tc = read(0, buf, BUFSIZ)) > 0) {
		for (int i = 0;i < tc;i++) {
			ch = buf[i];
			if (ch >= lettera && ch <= letterz) ch = (((ch-lettera)+13)%26)+lettera;
			else if (ch >= letterA && ch <= letterZ) ch = (((ch-letterA)+13)%26)+letterA;
			else if (ch >= number0 && ch <= number9) ch = (((ch-number0)+5)%10)+number0;
			buf[i] = ch;
		}
		write(1, buf, tc);
	}
	return 0;
}
