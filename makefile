# Application : rot18 cypher cli program
#	Description
# Author Raymond Thomson

all: help

CC=gcc
MAN=man/
SRC=src/
CFLAGS=-I.
LIBS=-lm

%.o: %.c
	@echo 'Invoking: GCC C object files'
	$(CC) -c -o $($SRC)$@ $< $(CFLAGS)

rot18: $(SRC)rot18.o
	@echo 'Building and linking target: $@'
	$(CC) -o rot18 $(SRC)*.o $(LIBS)

local: rot18 clean

clean:
	@echo 'Cleaning build objects'
	rm -f $(SRC)*.o 
	@echo 'Installed. Enter ./rot18 to run'

install: rot18 
	@echo 'Installing'	
	cp rot18 /usr/local/bin/rot18
	cp $(MAN)rot18 /usr/share/man/man1/rot18.1
	gzip /usr/share/man/man1/rot18.1
	@echo 'Cleaning build objects'
	rm -f $(SRC)*.o 
	rm -f rot18
	@echo 'installed, type rot18 to run or man rot18 for the manual'

remove:
	rm -f rot18

uninstall:
	rm -f /usr/local/bin/rot18
	rm -f /usr/share/man/man1/rot18.1.gz
	@echo 'rot18 uninstalled.'

help:
	@echo 'Make options for rot18'
	@echo 'Local Folder make'
	@echo '    make local'
	@echo 'Local Folder uninstall'
	@echo '    make remove'
	@echo 'System Install'
	@echo '    sudo make install'
	@echo 'System Uninstall'
	@echo '    sudo make uninstall'

	
